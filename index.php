<?php

require_once './classes/Empresa.php';
require_once './classes/Oportunitat.php';
require_once './classes/CarpetaAccio/Accio.php';

$tipusAccio = new TipusAccio('"Quin tipus eeehhh!?!?!?"');
$empresa = new Empresa(1, "EmpresaProva", "Empresa de proves SLU", "987654321A");
$oportunitat = new Oportunitat(1, "Oportunitat de proves", "Categoria màgica");
$accio = new Accio(1, "The final action", $tipusAccio);
$Action = new Accio(1, "Acció de test", "!?!?!?\Quin tipus eeehhh!?!?!?");

$textEmpresa = $empresa->toString();
$textOportunitat = $oportunitat->toString();
$textAccio = $accio->toString();

echo "<h1>{$textEmpresa}</h1>";
echo "<h3>{$textOportunitat}</h3>";
echo "<h5>{$textAccio}</h5>";
echo "<span>" . $empresa->toString() . "</span>";
echo "<h3>" . $oportunitat->toString() . "</h3>";
echo "<div>" . $Action->toString() . "</div>";