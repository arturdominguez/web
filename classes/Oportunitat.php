<?php

require_once './classes/OportunitatsCategories.php';

class Oportunitat
{
    private $idOportunitat;
    private $nomOportunitat;
    private $categoriaOportunitat;

    public function __construct(int $idOportunitat, string $nomOportunitat, string $categoriaOportunitat)
    {
        $this->idOportunitat = $idOportunitat;
        $this->nomOportunitat = $nomOportunitat;
        $this->categoriaOportunitat = new OportunitatCategoria($categoriaOportunitat);
    }

    public function toString() {
        return "Id: " . $this->idOportunitat . " | Nom: " . $this->nomOportunitat . " | Categoria: " . $this->categoriaOportunitat->getNomOportunitatCategoria();
    }
}