<?php

class TipusAccio
{
    private $nomTipusAccio;

    public function __construct($tipus)
    {
        $this->nomTipusAccio = $tipus;
    }

    public function toString() : string
    {
        return $this->nomTipusAccio;
    }
}