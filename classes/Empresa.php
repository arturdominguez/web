<?php

class Empresa
{
    private $idEmpresa;
    private $nomEmpresa;
    private $nomFiscalEmpresa;
    private $nifEmpresa;

    public function __construct(int $idEmpresa, string $nomEmpresa, string $nomFiscalEmpresa, string $nifEmpresa)
    {
        $this->idEmpresa = $idEmpresa;
        $this->nomEmpresa = $nomEmpresa;
        $this->nomFiscalEmpresa = $nomFiscalEmpresa;
        $this->nifEmpresa = $nifEmpresa;
    }

    public function toString() {
        return "Id: " . $this->idEmpresa . " | Nom: " . $this->nomEmpresa;
    }
}