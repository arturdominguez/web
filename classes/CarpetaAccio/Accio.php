<?php

class Accio
{
    private $idAccio;
    private $nomAccio;
    private $tipusAccio;

    public function __construct(int $idAccio, string $nomAccio, TipusAccio $tipusAccio)
    {
        $this->idAccio = $idAccio;
        $this->nomAccio = $nomAccio;
        $this->tipusAccio = $tipusAccio;
    }

    public function toString(): string
    {
        $textTipusAccio = $this->tipusAccio->toString();
        return "Nom: {$this->nomAccio} | Tipus: {$textTipusAccio}";
    }
}