<?php

class OportunitatCategoria{

    private $idOportunitatCategoria;
    private $nomOportunitatCategoria;

    public function __construct($nomOportunitatCategoria)
    {
        $this->nomOportunitatCategoria = $nomOportunitatCategoria;
    }

    public function getNomOportunitatCategoria(){
        return $this->nomOportunitatCategoria;
    }

}